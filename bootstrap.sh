#!/bin/bash
# This script will download and start Apache, and then create a
# symlink between your synced files directory and the location where
# Apache will look for the content
apt-get update
apt-get install -y apache2

if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant /var/www
fi

# Install python 3.9
apt update
apt install -y software-properties-common
add-apt-repository ppa:deadsnakes/ppa
apt install -y python3.9.0
apt install python3.9-distutils

#Install Postgres
apt-get update
apt-get install -y postgresql postgresql-contrib

# Install pipenv
apt-get install -y python3-pip
pip3 install pipenv

# Install git
apt-get update
apt-get install -y git-all
cd /var/www
git clone https://github.com/sontiveros-AT/Compiler_AT_Project.git
cd Compiler_AT_Project
git checkout develop

# Create jalacompilerdb database
sudo -u postgres createdb jalacompilerdb

# Install pipfile pinned packages
pipenv install --ignore-pipfile

# Make and run migrations
pipenv run python manage.py makemigrations
pipenv run python manage.py migrate

# Run server
pipenv run python manage.py runserver